package com.dexciuq.gallery_animation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import com.dexciuq.gallery_animation.databinding.FragmentPictureBinding
import com.dexciuq.gallery_animation.model.Picture

private const val ARG_ID = "id"
private const val ARG_IMAGE = "image"

class PictureFragment : Fragment() {

    private val binding by lazy { FragmentPictureBinding.inflate(layoutInflater) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        arguments?.let { args ->
            binding.image.setImageResource(args.getInt(ARG_IMAGE))
        }
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(picture: Picture) = PictureFragment().apply {
            arguments = bundleOf(
                ARG_ID to picture.id,
                ARG_IMAGE to picture.image
            )
        }
    }
}