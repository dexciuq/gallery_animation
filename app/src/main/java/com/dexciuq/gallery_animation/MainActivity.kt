package com.dexciuq.gallery_animation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dexciuq.gallery_animation.adapter.GalleryTransformer
import com.dexciuq.gallery_animation.adapter.GalleryViewPagerAdapter
import com.dexciuq.gallery_animation.data.LocalDataSource
import com.dexciuq.gallery_animation.databinding.ActivityMainBinding
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val pictureList = LocalDataSource.pictureList
        val adapter = GalleryViewPagerAdapter(this, pictureList)

        binding.viewPager.adapter = adapter
        binding.viewPager.currentItem = pictureList.size / 2
        binding.viewPager.setPageTransformer(GalleryTransformer())
        binding.viewPager.offscreenPageLimit = 1

        TabLayoutMediator(binding.tabLayout, binding.viewPager)  { _, _ -> }.attach()
    }
}