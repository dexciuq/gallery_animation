package com.dexciuq.gallery_animation.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.dexciuq.gallery_animation.PictureFragment
import com.dexciuq.gallery_animation.model.Picture

class GalleryViewPagerAdapter(
    fragmentActivity: FragmentActivity,
    private val pictureList: List<Picture>,
) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = pictureList.size

    override fun createFragment(position: Int): Fragment {
        val picture = pictureList[position]
        return PictureFragment.newInstance(picture)
    }
}