package com.dexciuq.gallery_animation.model

import androidx.annotation.DrawableRes

data class Picture(
    val id: Long,
    @DrawableRes val image: Int,
)
