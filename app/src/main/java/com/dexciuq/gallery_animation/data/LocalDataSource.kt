package com.dexciuq.gallery_animation.data

import com.dexciuq.gallery_animation.R
import com.dexciuq.gallery_animation.model.Picture

object LocalDataSource {
    val pictureList = listOf(
        Picture(id = 1, image = R.drawable.ic_nature_1),
        Picture(id = 2, image = R.drawable.ic_nature_2),
        Picture(id = 3, image = R.drawable.ic_nature_3)
    )
}